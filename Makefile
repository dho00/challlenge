POSTGRES_CONTAINER ?= postgres-challenge
POSTGRES_IMAGE ?= postgres:11
CHALLENGE_DB ?= challenge
POSTGRES_PASSWORD ?= azerty789
POSTGRES_BIND_PORT ?= 5432
BIND_PORT ?= -p ${POSTGRES_BIND_PORT}:5432

run_postgres_local:
	docker run -d \
	--name ${POSTGRES_CONTAINER} \
	-e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} \
	-e POSTGRES_DB=${CHALLENGE_DB} \
	${BIND_PORT} \
	${POSTGRES_IMAGE}

run_postgres:
	docker run -d \
	--name ${POSTGRES_CONTAINER} \
	-e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} \
	-e POSTGRES_DB=${CHALLENGE_DB} \
	${POSTGRES_IMAGE}

run_local:
	virtualenv -p python3.7 venv
	./venv/bin/python3.7 -m pip install -r requirements.txt
	./migration.sh local
	./venv/bin/python3.7 challenge/manage.py import challenge/app/tests/fixtures
	echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', '', 'admin')" | ./venv/bin/python3.7 challenge/manage.py shell || true
	./venv/bin/python3.7 challenge/manage.py runserver 0.0.0.0:8000

run: run_postgres_local run_local

clean_migrations:
	ls challenge/app/migrations/* | grep -v __init__.py | xargs rm || true
	rm -rf challenge/app/migrations/__pycache__ || true

destroy_postgres:
	docker rm -f ${POSTGRES_CONTAINER} || true

clean: clean_migrations destroy_postgres
	rm -rf venv
