# challenge

## Backend
The backend is a GraphQL API built with Django + Postgres  + Graphene

To launch the backend:

1- Using make (not available for the moment):

- `sudo apt-get install python3 python-dev python3-dev \
     build-essential libssl-dev libffi-dev \
     libxml2-dev libxslt1-dev zlib1g-dev \
     python-pip`
- `make run`

2- Manually:

- start a postgres image `docker run --name postgres -e POSTGRES_PASSWORD=azerty789 -d -p 5432:5432 postgres`
- create the database `docker exec -it postgres bash` then `psql -U postgres` and finally `create database challenge;`
- It's preferable to use a virtual env to install python dependencies virtualenv -p python3.7 <env_name> (make sure you have virtualenv installed)
- Activate the virtualenv source <env_name>/bin/activate
- install requirements `pip install -r requirements.txt`
- make migrations  `python challenge/manage.py makemigrations`

- run migrations `python challenge/manage.py migrate`
- import dump data `python challenge/manage.py loaddata challenge/app/tests/data/db.json`
- flush data `python challenge/manage.py flush`
- run the server `python challenge/manage.py runserver `


go to localhost:8000/graphql to test queries

 example of a query :
 ```
```
## Authentification

To create a user run `python challenge/manage.py createsuperuser` and follows the instructions

go to `localhost:8000/admin/` to login


# Front

The front is a React app to run it `cd front && yarn` then `yarn start`
