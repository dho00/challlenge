import gql from 'graphql-tag';

export default gql`
query getAggStationState {
    getAggStationState {
        name
        value
    }
}
`;
