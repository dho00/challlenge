import gql from 'graphql-tag';

export default gql`
query getNearest {
    getNearest {
        id
        stationCode
        stationName
        stationState
        nbfreedock
    }
}
`;
