import gql from 'graphql-tag';

export default gql`
query getAggVelibType {
    getAggVelibType {
        name
        value
    }
}
`;
