import QUERY_ALL_VELIBS from './allVelibs';
import QUERY_AGG_STATION_STATE from './getAggStationState';
import QUERY_AGG_VELIB_TYPE from './getAggVelibType';
import QUERY_NEAREST from './getNearest';

export {
    QUERY_ALL_VELIBS,
    QUERY_AGG_STATION_STATE,
    QUERY_AGG_VELIB_TYPE,
    QUERY_NEAREST,
}