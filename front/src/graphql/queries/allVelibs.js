import gql from 'graphql-tag';

export default gql`
query allVelibs(
$orderBy:String,
$first:Int,
$last:Int,
$after:String,
$before:String,
$stationNameContains:String){
    allVelibs(
        orderBy:$orderBy,
        first:$first,
        last:$last,
        after:$after,
        before:$before,
        stationNameContains:$stationNameContains
    ) {
        pageInfo {
            startCursor
            endCursor
        },
        totalCount
        edgeCount
        edges {
            node {
                id
                stationCode
                stationName
                stationState
                nbfreedock
                overflowactivation
                nbedock
                nbbikeoverflow
                nbbike
                nbebike
                creditcard
                kioskstate
                geo
                timestamp
            }
        }
    } 
}
`;
