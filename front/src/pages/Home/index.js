import React from 'react';
import Grid from '@material-ui/core/Grid';
import DatePicker from 'components/DatePicker'
import PieChartContainer from 'containers/PieChart.container';
import BarChartContainer from 'containers/BarChart.container';
import AllVelibsContainer from 'containers/allvelibsTable.container';
import GeoAllVelibsContainer from 'containers/geoallvelibs.container';
import { withStyles } from "@material-ui/core/styles";
import {FlyToInterpolator} from 'deck.gl';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    margin: '2em',
    marginTop: '7em',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class MainPage extends React.Component {

  constructor() {
    super()

    this.state = {
      increment: 500,
      viewState : {
        latitude: 48.8612,
        longitude: 2.3522,
        zoom: 12,
        pitch: 0,
        bearing: 0
      },
      hovered: {}
    }
    // On bind la méthode afin de passer le contexte à la méthode (this) à la place on peut faire un fat arrow
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._renderTooltip = this._renderTooltip.bind(this);
    this._onClick = this._onClick.bind(this)
  }

  _onViewStateChange ({ viewState }) {
    this.setState({ viewState });
  }

  // methode de fat arrow afin de passer le contexte(this) à la fonction à la plce on peut bind
  _goToBubble = (x,y) => {
    this.setState({
      viewState: {
        ...this.state.viewState,
        longitude: x,
        latitude: y,
        zoom: 14,
        pitch: 0,
        bearing: 0,
        transitionDuration: 2000,
        transitionInterpolator: new FlyToInterpolator()
      }
    });    
  }

  _renderTooltip() {
    const {hoveredObject, pointerX, pointerY} = this.state.hovered || {};
    return hoveredObject && (
      <div style={{position: 'absolute', zIndex: 1, pointerEvents: 'none', left: pointerX, top: pointerY, backgroundColor: '#ffffff'}}>
        <div>Station: { hoveredObject.properties.station_name }</div>
        <div>Etat: { hoveredObject.properties.station_state }</div>
        <div>Borne disponible: { hoveredObject.properties.nbedock }</div>
        <div>Nb vélo: { hoveredObject.properties.nbbike }</div>
        <div>Nb vélo électrique: { hoveredObject.properties.nbebike }</div>        
      </div>
    );
  };

  _onHover = (info) => {
      this.setState({
        hovered: {
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y
      }
    })
  }

  _onClick(row) {
    const [x, y] = row.values[6].value[0].replace('(','').replace(')','').split(' ')
    this._goToBubble(parseFloat(y), parseFloat(x))
  }

  render() {
    const {classes} = this.props
    return (
    <div className={classes.root}>
      <Grid container spacing={3}>
          <Grid item xs={12}>
            <DatePicker />
          </Grid>
          <Grid item xs={4}>
            <PieChartContainer />
          </Grid>
          <Grid item xs={4}>
            <BarChartContainer />
          </Grid>
          <Grid item xs={4}>
            <GeoAllVelibsContainer
              viewState={this.state.viewState}
              _onViewStateChange={this._onViewStateChange}
              _onHover={this._onHover}
              _renderTooltip={this._renderTooltip}
            />
          </Grid>
          <Grid item xs={12}>
            <AllVelibsContainer 
              _onClick={this._onClick}
            />
          </Grid>
        </Grid>
      </div>
    )  
  }
}

export default withStyles(styles, { withTheme: true })(MainPage);
