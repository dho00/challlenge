import React from 'react';
import config from 'config';
import axios from 'axios';
import LoginArea from 'components/LoginArea';
import commonSettings from 'settings/common';

axios.defaults.withCredentials = true

class Login  extends React.Component {
  constructor() {
    super();
    this.state = {
      error: '',
    };
  }

  handleSubmit = (username, password) => {
    if (!username) {
      return this.setState({ error: 'Nom d\'utilisateur requis' });
    }

    if (!password) {
      return this.setState({ error: 'Mot de passe requis' });
    }
    axios.post(config.backendURL + "/auth/login/",
    {username: username, password: password},
    {headers:{"content-type":"application/json"},
    }
    )
    .then(() => {
      document.location.href = this.props.location ? (this.props.location.state ? this.props.location.state.referrer : '/') : '/';
    })
    .catch(err => {
      console.log(err)
      this.setState({error: "Identifiant incorrect"})
    })        
  }
    
  render() {
    return (
      <div className="Login">
        <LoginArea
          {...commonSettings.login}
          errorMessage={this.state.error}
          onSubmitLogin={this.handleSubmit}
        />
      </div>
    );
  }  
}

export default Login