import config from 'config';
import axios from 'axios';
import { getCookie, destroyCookie } from 'utils/csrf_cookie';

export default () => {
  const csrftoken = getCookie('csrftoken');
  axios.post(config.backendURL + "/auth/logout/",
  {},
  {headers:{"content-type":"application/json","X-CSRFToken": csrftoken }})
  .then(resp => {
    destroyCookie('csrftoken');
    console.log(resp)
    document.location.href = '/login';
  })
}
