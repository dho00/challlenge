export default (data) => {
    return data.getAggVelibType.map(item => {
        return({        
            name: item.name,
            value: item.value,
            color: item.name === 'bike' ? '#cc493f' : '#cc3fc2',
        })
    })
    
}