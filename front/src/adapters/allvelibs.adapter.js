export default (data) => {
    const regex = /\((.*?)\)/g;
    return data.allVelibs.edges.map((item, index) => {
        return(
        {        
            values: [
                {value: item.node.stationCode},
                {value: item.node.stationName},
                {value: item.node.stationState},
                {value: String(item.node.nbfreedock)},
                {value: item.node.nbbike},
                {value: item.node.nbebike},
                {value: item.node.geo.match(regex)},
            ],
            onRowClick: () => {},
            key: index,
        })
    })
    
}