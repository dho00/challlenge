export default (data) => {
    return data.getAggStationState.map(item => {
        return({        
            name: item.name,
            value: item.value,
            color: item.name === 'Close' ? '#cc493f' : (item.name === 'Operative' ? '#3fcc49' : '#cc3fc2'),
        })
    })
    
}