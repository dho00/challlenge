export default (data) => {
    const regex = /\((.*?)\)/g;
    const features =  data.allVelibs.edges.map(item => {
        const coors = item.node.geo.match(regex)[0].replace('(','').replace(')','').split(' ');
        return {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        parseFloat(coors[1]),
                        parseFloat(coors[0])
                    ]
                },
                "properties": {
                    "nbfreeedock": item.node.nbfreedock,
                    "station_state": item.node.stationState,
                    "overflowactivation": item.node.overflowactivation,
                    "station_code": item.node.stationCode,
                    "nbedock": item.node.nbedock,
                    "nbbikeoverflow": item.node.nbbikeoverflow,
                    "nbbike": item.node.nbbike,
                    "nbebike": item.node.nbebike,
                    "station_name": item.node.stationName,
                    "creditcard":item.node.creditcard,
                    "geo": [
                        parseFloat(coors[1]),
                        parseFloat(coors[0])
                    ],
                    "kioskstate": item.node.kioskstate,
                }
            }
        })
    return {
        "type": "FeatureCollection",
        features,
    }
}