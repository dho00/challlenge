import React from 'react';
import { Route, Switch, Redirect, useHistory } from 'react-router-dom';
import TopBar from 'containers/topBar.container'
import {
  HomePage,
  LoginPage,
} from './pages';
import { getCookie } from 'utils/csrf_cookie';

const RouteToOrRedirect = ({ path, component : Component, condition, redirect, exact}) => (
  <Route
    exact={exact}
    path={path}
    render={props => (condition ? (
      <Component {...props} />
    ) :
    (
      <Redirect
        to={{
          pathname: redirect,
          state: { referrer: props.location.pathname },
        }}
      />
    ))
    }
  />
);

const noTopBarPages = [
  '/login',
  '/health'
]

export default () => (
  <React.Fragment>
    {
      (getCookie('csrftoken') && !noTopBarPages.includes(useHistory().location.pathname)) &&
      <div style={{ marginBottom: 80 }}>
        <TopBar />
        Test
      </div>
    }
    <Switch>
      <RouteToOrRedirect exact path='/login' component={LoginPage} condition={!getCookie('csrftoken')} redirect='/' />
      <RouteToOrRedirect exact path='/' component={HomePage} condition={getCookie('csrftoken')} redirect='/login' />
      <RouteToOrRedirect exact path='*' component={HomePage} condition={getCookie('csrftoken')} redirect='/login' />
    </Switch>
  </React.Fragment>
);
