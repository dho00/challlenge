export default {
    loadingMessage: 'En attente',
    emptyTableMessage: 'Aucun résultat',
    emptyTableStyle: {
      textAlign: 'center', 
      padding: 50,
    },
    tableHeader: {
      style: {
        backgroundColor: '#6C84A4',
        color: '#03D1CE',
        textAlign: 'center',
        alignItems: 'center',
        borderBottom: 'none',
        paddingTop: 20,
        fontSize: 14,
        fontWeight: 400
      },
      data: [
        {
          value: 'Code de la station',
          key: 'stationcode',
          sort: true,
        },
        {
          value: 'Nom de la station',
          sort: true,
          key: 'stationname',
        },
        {
          value: 'Etat de la station',
          sort: true,
          key: 'station_state',
          style: {
            width: '10%',
          },
        },
        {
          value: 'Nombre de borne disponible',
          key: 'num_deposit',
          style: {
            width: '10%',
          },
        },
        {
          value: 'Nombre de vélo mécanique',
          key: 'nbbike',
          style: {
            width: '10%',
          },
        },
        {
          value: 'Nombre de vélo électrique',
          key: 'nbebike',
          style: {
            width: '10%',
          },
        },
        {
          value: 'Coordonnées',
          key: 'geo',
          style: {
            width: '10%',
          },
        },
      ],
      sorting: {
        sort_key: '',
        sort_type: ''
      }
    },
    tableData: {
        style: {
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: 13,
          },
          rows: []
    },
}
