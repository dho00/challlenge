import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { ApolloLink, Observable, split } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { withClientState } from 'apollo-link-state';
import { HttpLink } from 'apollo-link-http';
import App from './App';
import config from './config';
import { getCookie } from 'utils/csrf_cookie';
import logout from 'utils/user';
import * as serviceWorker from './serviceWorker';
import './index.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Theme from './Theme';


const wsLink = new WebSocketLink({
    uri: config.backendWSURL+'ws/',
});
  
const splitLink = split(
    ({ query }) => {
        console.log(query)
        const { kind, operation } = getMainDefinition(query)
        return kind === 'OperationDefinition' && operation === 'subscription'
    },
    wsLink,
    new HttpLink({
        uri: config.backendURL+'/graphql/',
        credentials: 'include',
    }),
);
  
const request = (operation) => {
    const csrftoken = getCookie('csrftoken');
    operation.setContext({
    headers: {
        "X-CSRFToken": csrftoken
    }
    });
}

const cache = new InMemoryCache();

const requestLink = new ApolloLink((operation, forward) =>
  new Observable(observer => {
    let handle;
    Promise.resolve(operation)
      .then(() => request(operation))
      .then(() => {
        handle = forward(operation).subscribe({
          next: observer.next.bind(observer),
          error: observer.error.bind(observer),
          complete: observer.complete.bind(observer),
        });
      })
      .catch(observer.error.bind(observer));

    return () => {
      if (handle) handle.unsubscribe();
    };
  })
);

const client = new ApolloClient({
    link: ApolloLink.from([
        onError(({ graphQLError, networkError }) => {
            if (graphQLError) {
            console.log('graphql error', graphQLError);
            }
            if (networkError) {
              if (networkError.statusCode === 403) {
                logout()
              }      
            console.log('network error', networkError);
            }
        }),
        requestLink,
        withClientState({
            cache,
        }),
        splitLink,
        ]),
    cache,
});

const theme = createMuiTheme(Theme);
ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </MuiThemeProvider>
    </ApolloProvider>
  </BrowserRouter>
  , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
