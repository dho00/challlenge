import React from 'react';
import renderer from 'react-test-renderer';
import CustomizableTable from './CustomizableTable.component';

const tableHeader = {
  style:{
    backgroundColor: '#6C84A4',
  },
  data: [
    { CellRenderer: () => (<span>Etat de l'analyse</span>) },
    { CellRenderer: () => (<span>Alerte</span>) },
    { CellRenderer: () => (<span>Patient</span>) },
    { CellRenderer: () => (<span>Prescription à statuer</span>) },
    { CellRenderer: () => (<span>En attente d'analyse depuis</span>) },
    { CellRenderer: () => (<span>Dernière modification</span>) },
    { CellRenderer: () => (<span>Secteur</span>) },
  ],
};

const tableData = {
  rows: [
    {
      values: [
        { value: 'Incomplète' },
        { value: '6' },
        { value: 'PIGNON Marie' },
        {
          value: '8 sur 12',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '6',
          unit: 'j'
        },
        { value: 'Cradiologie 1' },
      ]
    },
    {
      values: [
        { value: 'Incomplète' },
        { value: '0' },
        { value: 'ROUBERT Jean' },
        {
          value: '8 sur 9',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '30',
          unit: 'min'
        },
        { value: 'Cradiologie 3' },
      ]
    },
    {
      values: [
        { value: 'Incomplète' },
        { value: '4' },
        { value: 'SAUL René' },
        {
          value: '2 sur 9',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '6',
          unit: 'j'
        },
        { value: 'Cradiologie 6' },
      ]
    },
    {
      values: [
        { value: 'Incomplète' },
        { value: '8' },
        { value: 'WATSON Fannie' },
        {
          value: '8 sur 12',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '2',
          unit: 'h'
        },
        { value: 'Gastrologie 2' },
      ]
    },
    {
      values: [
        { value: 'Incomplète' },
        { value: '1' },
        { value: 'RADBURY Georges' },
        {
          value: '0 sur 12',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '1',
          unit: 'j'
        },
        { value: 'Gastrologie 2' },
      ]
    },
    {
      values: [
        { value: 'Complète' },
        { value: '0' },
        { value: 'PERSON Jack' },
        {
          value: '0 sur 12',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '2',
          unit: 'j'
        },
        { value: 'Gastrologie 10' },
      ]
    },
    {
      values: [
        { value: 'Complète' },
        { value: '0' },
        { value: 'PATOULATCHI Marc' },
        {
          value: '0 sur 5',
          unit: 'prescriptions'
        },
        {
          value: '10',
          unit: 'j'
        },
        {
          value: '2',
          unit: 'j'
        },
        { value: 'Gastrologie 1' },
      ]
    },
  ],
};

const props = {
  tableHeader,
  tableData,
  emptyTableMessage: 'Choisissez votre service pour afficher des patients'
};

test('CustomizableTable default', () => {
  const component = renderer.create(<CustomizableTable {...props} />).toJSON();
  expect(component).toMatchSnapshot();
});
