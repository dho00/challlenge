import React from 'react';
import PropTypes from 'prop-types';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Paper from "@material-ui/core/Paper";
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Badge from '@material-ui/core/Badge';
import Notifications from '@material-ui/icons/Notifications';
import debounce from "lodash.debounce";
import { withStyles } from "@material-ui/core/styles";
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';


const styles = () => ({
  root: {
    overflowX: 'auto',
    paddingTop: 0,
  },
  tooltip: {
    maxWidth: 200,
    fontSize: 13,
    padding: 10,
    wordWrap: 'break-word'
  },
  tableHeadDiv: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    userSelect: 'none',
    justifyContent: 'center',
    color: '#FFFFFF'
  },
  dropUpIcon: {
    cursor: 'pointer',
    position: 'absolute',
    fontSize: 12,
    width: 0,
    height: 0,
    border: '6px solid transparent',
    borderBottom: '6px solid #03D1CE',
    marginTop: -10,
    '&:hover': {
      borderBottom: '6px solid #38B2B0',
    },
  },
  dropDownIcon: {
    cursor: 'pointer',
    position: 'absolute',
    fontSize: 12,
    width: 0,
    height: 0,
    border: '6px solid transparent',
    borderTop: '6px solid #03D1CE',
    marginTop: 5,
    '&:hover': {
      borderTop: '6px solid #38B2B0',
    },
  },
  tableCell: {
    padding: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tableBodyRow: {
    '&:hover': {
      backgroundColor: '#F5F5F5'
    }
  },
  tooltipPlacementBottom: {
    marginTop: -15,
  },
  selectCheckbox: {
    padding: 0
  }
});

class CustomizableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.tableData.rows.slice(0),
      sorting: this.props.tableHeader.sorting,
      selectedRow: this.props.selectedRow || null,
      checkedRows: [],
      checkedCell: false,
      showCheckBox: false,
      cellIndex: null,
    };

    window.onscroll = this.props.onScrollToBottom && debounce(() => {
      ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) && this.props.onScrollToBottom();
    }, 10);
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      selectedRow: nextProps.selectedRow
    }
  }

  handleOnSort = (key, type) => {
    this.setState({
      sorting: {
        sort_key: key,
        sort_type: type,
      }
    })
    this.props.tableHeader.onSort && this.props.tableHeader.onSort({
      sort_key: key,
      sort_type: type,
    });
  }

  handleMouseHover = (index) => {
    this.setState({
      cellIndex: index,
      showCheckBox: true
    });
  }

  handleMouseLeave = () => {
    this.setState({
      showCheckBox: false
    });
  }

  handleClick = (row, index) => {
    this.setState({
      selectedRow: (index === this.state.selectedRow) ? null : index,
      cellIndex: (index !== this.state.selectedRow) && index
    })
    row.onRowClick((index === this.state.selectedRow) ? undefined : {
      index,
      key: row.key,
      values: row.values, 
    });
  }

  handleCheckboxClick = async(event, row, index) => {
    event.stopPropagation();
    await this.state.checkedRows.map(a => a.index).some(b => b === index) ?
    this.setState({
      checkedRows: this.state.checkedRows.filter(i => i.index !== index)
    }) : this.setState({
      checkedRows: [...this.state.checkedRows, {
        'index': index,
        'values': row.values
      }]
    })
    this.props.tableData.onCheckboxClick(this.state.checkedRows);
  }

  componentDidUpdate(prevProps) {    
    if (prevProps.tableData !== this.props.tableData) {
      this.setState({
        data: this.props.tableData.rows.slice(0),
        sorting: this.props.tableHeader.sorting,
      })
      window.onscroll = this.props.onScrollToBottom && debounce(() => {
        ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) && this.props.onScrollToBottom();
      }, 10);
    }
  }

  render() {
    const { classes, rootStyle, tableHeader, tableData, emptyTableMessage, isLoading, loadingMessage, emptyTableStyle, _onClick } = this.props;
    const { sorting } = this.state;

    const renderTableCell = (CellRenderer, value, style, sort, key, tooltip, index) => (
      <TableCell key={index} className={classes.tableCell} style={{...tableHeader.style, ...style}}>
        <div className={classes.tableHeadDiv} >
          {value || <CellRenderer />}
          {sort && (
            <div style={{ position: 'relative', marginLeft: 15 }}>
              {(!sorting || sorting.sort_key !== key || sorting.sort_type === 'asc') && (
                <div
                  onClick={() => {
                  (sorting.sort_type === 'asc' && sorting.sort_key === key) ?
                    this.handleOnSort() : this.handleOnSort(key, 'asc')
                  }}
                  className={classes.dropUpIcon}></div>
              )}
              {(!sorting || sorting.sort_key !== key || sorting.sort_type === 'desc') && (
                <div
                  onClick={() => {
                    (sorting.sort_type === 'desc' && sorting.sort_key === key) ?
                      this.handleOnSort() : this.handleOnSort(key, 'desc')
                    }}
                  className={classes.dropDownIcon}></div>
              )}
            </div>
          )}
        </div>
      </TableCell>
    );

    return (
    <Paper className={classes.root} style={{...rootStyle}} data-cy="table">
        <Table className={classes.table} style={{ tableLayout: 'fixed' }}>
          <TableHead>
            <TableRow>
              {
                tableData.checkBoxOnHover &&
                <TableCell className={classes.tableCell} style={{...tableHeader.style, minWidth: 12}}> </TableCell>
              }
              {tableHeader.data.map(({ CellRenderer, value, style, sort, key, tooltip }, index) => (
                tooltip ?
                <Tooltip
                  key={index}
                  title={tooltip}
                  placement="top"
                  classes={{ tooltip: classes.tooltip, tooltipPlacementTop: classes.tooltipPlacementTop }}>
                  { renderTableCell(CellRenderer, value, style, sort, key, tooltip, index) }
                </Tooltip>
                : renderTableCell(CellRenderer, value, style, sort, key, tooltip, index)
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.data.length > 0 && this.state.data.map((row, index) => (
              <TableRow
                key={index}
                className={classes.tableBodyRow}
                style={{ opacity: row.hasNoAlertAndPrescription ? 0.5 : 1,
                  borderLeft: row.newRow ? '5px solid #03D1CE' : '',
                  cursor: row.onRowClick ? 'pointer' : 'default',
                  backgroundColor: (this.state.selectedRow === index) && '#FCF3F3',
                }}
                onClick={() => { row.onRowClick && _onClick(row); this.handleClick(row, index); }}
                onMouseEnter={() => this.handleMouseHover(index)}
                onMouseLeave={this.handleMouseLeave}>
                  {
                    (tableData.checkBoxOnHover) &&
                    <TableCell className={classes.selectCheckbox}>
                      {
                        ((this.state.showCheckBox && this.state.cellIndex === index) || this.state.selectedRow === index
                          || (this.state.checkedRows.map(a => a.index).some(c => c === index))) &&
                        <Checkbox
                          checked={this.state.checkedRows.map(a => a.index).some(c => c === index) ? true : false}
                          onClick={(event) => this.handleCheckboxClick(event, row, index)}
                          value="checkedCell"
                          inputProps={{
                            'aria-label': 'primary checkbox',
                          }}
                          color="primary"
                          disableRipple
                        />
                      }
                    </TableCell>
                  }
                {row.values.map(({ notification, CellRenderer, value, tooltip, style, unit }, i) => (
                  (tooltip && !notification) ? (
                    <Tooltip key={i} title={tooltip} classes={{ tooltip: classes.tooltip, tooltipPlacementBottom: classes.tooltipPlacementBottom }} placement="bottom" >
                      <TableCell
                        className={classes.tableCell}
                        style={{
                          ...tableData.style,
                          ...style,
                        }}>
                        {
                          value ? (unit ? `${value} ${unit}` : value) : ( CellRenderer && <CellRenderer />)
                        }
                      </TableCell>
                    </Tooltip>
                  ) : (
                    <TableCell
                      key={i}
                      className={classes.tableCell}
                      style={{
                        ...tableData.style,
                        ...style,
                      }}>
                      {
                        notification ?
                          (value > 0) &&
                          <Tooltip
                            title={tooltip}
                            classes={{ tooltip: classes.tooltip }}
                            placement="right-start">
                            <Badge badgeContent={value} style={{ backgroundColor: 'none', color: '#4A4A4A' }}>
                              <Notifications />
                            </Badge>
                          </Tooltip>
                        : (value ? (unit ? `${value} ${unit}` : value) : ( CellRenderer && <CellRenderer />))
                      }
                    </TableCell>
                  )
                ))}
              </TableRow>
            ))}
            {this.state.data.length > 0 && isLoading && (
              <TableRow>
                <TableCell colSpan={tableHeader.data.length}>
                  <Grid container spacing={8} justify="center">
                    <Grid item xs/>
                    <Grid item xs={6} style={{ textAlign: 'center'}} >
                      {
                        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                          <Typography variant="body2" gutterBottom style={{ color: 'rgba(63,62,62,0.6)' }}> {loadingMessage} </Typography>
                          <CircularProgress size={12} style={{marginLeft: 10, color: 'rgba(63,62,62,0.6)'}}/>
                        </div>
                      }
                    </Grid>
                    <Grid item xs/>
                  </Grid>
                </TableCell>
              </TableRow>
            )}
            {this.state.data.length === 0 && (
              <TableRow>
                <TableCell colSpan={tableHeader.data.length}>
                  <Grid container spacing={8} justify="center">
                    <Grid item xs/>
                    <Grid item xs={6} style={{ ...emptyTableStyle }} >
                      {
                        isLoading ?
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                          <Typography variant="h3" gutterBottom style={{ color: 'rgba(63,62,62,0.6)' }}> {loadingMessage} </Typography>
                          <CircularProgress size={20} style={{marginLeft: 10, color: 'rgba(63,62,62,0.6)'}}/>
                        </div>
                        :
                        <Typography variant="h3" gutterBottom style={{ color: 'rgba(63,62,62,0.6)' }}>
                          {emptyTableMessage}
                        </Typography>
                      }
                    </Grid>
                    <Grid item xs/>
                  </Grid>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
        <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component="div"
            count={this.props.totalRows}
            rowsPerPage={this.props.rowsPerPage}
            page={this.props.page}
            onChangePage={(event, page,) => {this.props.handleChangePage(event, page, this.props.cursors)}}
            onChangeRowsPerPage={(event) => {this.props.handleChangeRowsPerPage(event)}}
          />
      </Paper>
    );
  }
}

CustomizableTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(CustomizableTable);
