import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import Logo from '../placeholder.png';

const styles = theme => ({
  fullPageGrid: {
    height: '100vh',
    color: '#253042',
    backgroundColor: '#F5F6FA',
  },
  logo: {
    textAlign: 'center',
    marginBottom: '3rem',
  },
  loginGrid: {
    height: '23rem',
    backgroundColor: '#FFFFFF',
    opacity: '2rem',
    textAlign: 'center',
    padding: '2rem'
  },
  connexion: {
    textAlign: 'center',
    marginBottom: '2rem',
    fontSize: 20,
  },
  loginButton: {
    height: 40,
    textTransform: 'none',
    marginBottom: '0.5rem',
  },
  gridButton:{
    marginTop: '2rem',
  },
  iconButton: {
    marginLeft: theme.spacing.unit,
    fontSize: '2.5rem',
  },
  copyright: {
    position: 'absolute',
    bottom: 0,
    textAlign: 'center',
    width: '100%',
  },
  linkForgetPassword: {
    cursor: 'pointer' ,
    textDecoration: 'underline'
  }
});

class Login  extends React.Component {
  state = {
    username: '',
    password: '',
    showPassword: false,
  };

  handleChangeUser = label => event => {
    this.setState({
      [label]: event.target.value,
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmitLogin(this.state.username, this.state.password);
  };

  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    })
  };

  render() {
    const {
      classes,
      styleLogo,
      errorMessage,
      onForgetPassword,
      inputStyles,
      inputLabelStyle,
      styleFormErros
    } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <Grid
          container
          alignItems="center"
          justify="center"
          alignContent="center"
          className={classes.fullPageGrid} >
          <Grid className={classes.logo} item xs={12}>
            <img data-cy="login-logo" style={styleLogo} alt="challenge" src={Logo} />
          </Grid>
          <Grid item sm={3} className={classes.loginGrid}>
            <Grid item className={classes.connexion}>
              <Typography variant="h4" gutterBottom>
                Connexion
              </Typography>
            </Grid>
            <TextField
              data-cy="login-identifiant"
              id="identifiant"
              label="Identifiant"
              placeholder="Entrez votre identifiant"
              value={this.state.username}
              onChange={this.handleChangeUser('username')}
              fullWidth
              margin="normal"
              error={errorMessage ? true : false}
              InputProps={{
                style: inputStyles
              }}
              InputLabelProps={{
                shrink: true,
                style: inputLabelStyle
              }}
            />
            <TextField
              data-cy="login-password"
              id="password"
              type={this.state.showPassword ? 'text' : 'password'}
              label="Mot de passe"
              placeholder="Entrez votre mot de passe"
              value={this.state.password}
              onChange={this.handleChangeUser('password')}
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
                style: inputLabelStyle
              }}
              InputProps={{
                style: inputStyles,
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={this.handleClickShowPassword}
                    >
                      {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              error={errorMessage ? true : false}
            />
            <FormHelperText data-cy="login-error" className={classes.formErrors} error={true} style={styleFormErros}>
              {errorMessage}
            </FormHelperText>
            <Grid item className={classes.gridButton}>
              <Button
                data-cy="login-submit"
                type="submit"
                className={classes.loginButton}
                variant="contained"
                color="primary">
                  Se connecter
              </Button>
              <div
                className={classes.linkForgetPassword}
                onClick={onForgetPassword}>
                <Typography
                  data-cy="login-forgot"
                  variant="caption"
                  display="block"
                  gutterBottom>
                  J’ai oublié mon mot de passe
                </Typography>
              </div>
            </Grid>
          </Grid>
          <Grid className={classes.copyright} item xs={12}>
            <Typography variant="caption" display="block" gutterBottom>
              Copyright tous droits réservés
            </Typography>
          </Grid>
        </Grid>
      </form>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Login);
