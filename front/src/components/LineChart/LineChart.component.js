import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts'

const data = [
  {
    timestamp: 'Page A', count: 4000, pv: 2400, amt: 2400,
  },
  {
    timestamp: 'Page B', count: 3000, pv: 1398, amt: 2210,
  },
  {
    timestamp: 'Page C', count: 2000, pv: 9800, amt: 2290,
  },
  {
    timestamp: 'Page D', count: 2780, pv: 3908, amt: 2000,
  },
  {
    timestamp: 'Page E', count: 1890, pv: 4800, amt: 2181,
  },
  {
    timestamp: 'Page F', count: 2390, pv: 3800, amt: 2500,
  },
  {
    timestamp: 'Page G', count: 3490, pv: 4300, amt: 2100,
  },
];

const LineChartComponent = (props) => {
  // const { data } = props;
    return (
        <LineChart width={600} height={300} data={data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
        <XAxis label={{ value: 'Timestamp', position: 'insideBottomRight', offset: 0 }} dataKey="timestamp" />
        <YAxis label={{ value: "Number", angle: -90, position: 'insideLeft' }}/>
        <CartesianGrid strokeDasharray="3 3"/>
        <Tooltip/>
        <Line type="monotone" dataKey="count" stroke="#8884d8" activeDot={{r: 8}}/>
      </LineChart>
    )
}

export default LineChartComponent;