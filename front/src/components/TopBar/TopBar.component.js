import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Logo from '../placeholder.png';

const styles = () => ({
  grow: {
    flexGrow: 1,
  },
  logo: {
    display: 'block',
    cursor: 'pointer',
  },
  search: {
    width: 200,
  },
  section: {
    display: 'flex',
    alignItems: 'center',
  },
  colorPrimaryTopBar: {
    backgroundColor: 'white',
  },
  rootList: {
    fontSize: 14,
    height: 14,
    color: '#4A4A4A',
    '&:hover': {
      backgroundColor: '#FAE8E8'
    },
  },
  iconButton: {
    cursor: 'pointer',
    width: 30,
    display: 'flex',
    marginLeft: 24,
  },
});

class TopBar extends React.Component {
  state = {
    open: false,
    displayDropdownIcon: 'none',
    anchorEl: null,
  };

  handleMenuOpen = (event) => {
    this.setState({ open: true });
    this.setState({ displayDropdownIcon: 'block' })
    this.setState({ anchorEl: event.currentTarget });
  }

  handleMenuClose = () => {
    this.setState({ open: false });
    this.setState({ displayDropdownIcon: 'none' });
    this.setState({ anchorEl: null });
  }

  handeLogout = () => {
    this.setState({ open: false });
    this.setState({ displayDropdownIcon: 'none' });
    this.props.onLogout();
  }

  handleFocusOnDropdownIcon = () => {
    this.setState({ displayDropdownIcon: 'block' });
  }

  handleOnMouseLeaveDropdownIcon = () => {
    this.state.open ? this.setState({ displayDropdownIcon: 'block' }) :
    this.setState({ displayDropdownIcon: 'none' })
  }

  render(){
    const {
      classes,
      onLogoClicked,
      menuStyle } = this.props;
    return (
      <div className={classes.grow} data-cy="topbar">
        <AppBar
          classes={{
            colorPrimary: classes.colorPrimaryTopBar
          }}
          data-cy="topbar"
          >
          <Toolbar>
            <div className={classes.logo} data-cy="topbar-logo" onClick={onLogoClicked}>
              <img style={{ width: 94 }} alt="challenge" src={Logo} />
            </div>
            <div className={classes.grow} />
            <div className={classes.section}>
              <div
                aria-haspopup="true"
                className={classes.iconButton}
                onClick={(event) => this.handleMenuOpen(event)}
                onMouseEnter={this.handleFocusOnDropdownIcon}
                onMouseLeave={this.handleOnMouseLeaveDropdownIcon}
                data-cy="dropdown-iconbutton"
              >
                <AccountCircle style={{ color: '#4A4A4A' }}/>
                <ArrowDropDownIcon
                  color='primary'
                  style={{ display: this.state.displayDropdownIcon }}/>
              </div>
              <Menu
                anchorEl={this.state.anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                id="menu-appbar"
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={this.state.open}
                onClose={this.handleMenuClose}
                style={menuStyle}
                data-cy="dropdown-menu"
              >
                <MenuItem
                  onClick={this.handeLogout}
                  classes={{
                    root: classes.rootList
                  }}>Se déconnecter</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(TopBar);
