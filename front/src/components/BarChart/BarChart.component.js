import React from 'react';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Cell
} from 'recharts';

const BarChartComponent = (props) => {
    const {data} = props;
    return (
        <BarChart width={600} height={300} data={data}
        margin={{top: 20, right: 30, left: 20, bottom: 5}}>
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="name"/>
            <YAxis/>
            <Tooltip/>
            <Bar dataKey="value" fill="#8884d8" label={{ position: 'top' }}>
            {
                data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={entry.color}/>
                ))
            }
       </Bar>
        </BarChart>
    );
}

export default BarChartComponent;
