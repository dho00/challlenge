import React from 'react';
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
    root: {
        width: '100%',
      },
      tableCell: {
        width: '50px',
        maxWidth: '50px',
      },
    controlPanel: {
        position: 'absolute',
        top: 0,
        left: 0,
        margin: '12px',
        padding: '20px',
        fontSize: '12px',
        lineHeight: 1.5,
        zIndex: 1,
        background: '#fff',
        fontFamily: 'Helvetica, Arial, sans-serif',
        boxShadow: '0 0 4px rgba(0, 0, 0, 0.15)',
    }
  });

const controlPanel = (props) => {
    const {classes, handleChange, val, toNY} = props;

    return (
    <div className={classes.controlPanel}>
        <div>
            <label>Radius</label>
            <input id="radius" type="range" min="0" max="1000" step="50" value={val} onChange={handleChange}></input>
            <span id="radius-value"></span>
        </div>
        {/* <div>
            <label>Coverage</label>
            <input id="coverage" type="range" min="0" max="1" step="0.1" value="1"></input>
            <span id="coverage-value"></span>
        </div>
        <div>
            <label>Upper Percentile</label>
            <input id="upperPercentile" type="range" min="90" max="100" step="1" value="100"></input>
            <span id="upperPercentile-value"></span>
        </div> */}
        <button type="button" onClick={toNY}>Click For NewYork!</button>
    </div>
    )
}

export default withStyles(styles)(controlPanel);
