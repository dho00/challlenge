import React from 'react';
import DeckGL from '@deck.gl/react';
import {StaticMap} from 'react-map-gl';
import {GeoJsonLayer} from '@deck.gl/layers';
// import ControlPanel from './controlPanel';
import { withStyles } from "@material-ui/core/styles";

// Set your mapbox access token here
const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiZGhvZGhvIiwiYSI6ImNrNjB0azJhbjAxNmozZXFxaDc5Y2c0MzQifQ.NLxypu5x-NdMNxIc-vQ-nQ'

const styles = () => ({
  root: {
    position: 'relative',
    display: "block",
    width: "100%",
    height: '400px',
  },
});

class Map extends React.Component {
  
  constructor() {
    super()
  }

  _fillColor(x) {
    if (x.station_state === 'Work in progress') {
      return [255, 102, 0, 200]
    }    
    else if (x.station_state === 'Operative') {
      return [0, 153, 51, 200]
    }
    else if (x.station_state === 'Close') {
      return [255, 51, 0, 200]
    }
    else {
      return [160, 160, 180, 200]
    }
  }

  render() {
    const { classes, data, viewState, _onViewStateChange, _onHover, _renderTooltip } = this.props

    const layer = new GeoJsonLayer({
      id: 'geojson-layer',
      data: data,
      pickable: true,
      stroked: false,
      filled: true,
      extruded: true,
      lineWidthScale: 20,
      lineWidthMinPixels: 2,
      getFillColor: info => this._fillColor(info.properties),
      getRadius: 36,
      getLineWidth: 1,
      getElevation: 30,
      // onClick: (info, event) => _onClick(info, event)
    });

    return (
      <div className={classes.root} >
        {/* <ControlPanel handleChange={this._handleChange} toNY={() => this._goToBubble(-74.1, 40.7)} val={this.state.increment}/> */}
        <DeckGL
          ref={deck => { this.deckGL = deck; }}
          viewState={viewState}
          controller={true}
          layers={[layer]}
          onViewStateChange={_onViewStateChange}
          onHover={_onHover}
        >
          <StaticMap mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN} />
          { _renderTooltip }
        </DeckGL>
      </div>
    );
  }
}
export default withStyles(styles, { withTheme: true })(Map);
