import React from 'react';
import SearchField from 'components/SearchField';
import CustomizableTable from 'components/CustomizableTable';
import Grid from '@material-ui/core/Grid';

const TableAndSearch = (props) => {
    const {page, rowsPerPage, handleChangePage, handleChangeRowsPerPage, totalRows, _onClick, handleSubmitForm, handleStateChange, setPage0, ...adaptedTableProps} = props;
    return (
        <Grid container spacing={1} alignItems="flex-end">
        <Grid item>
          <SearchField 
          handleSubmitForm={handleSubmitForm}
          handleStateChange={handleStateChange}
          setPage0={setPage0}
          />
        </Grid>
        <Grid item>
          <CustomizableTable
            {...adaptedTableProps }
            page={page}
            rowsPerPage={rowsPerPage}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            totalRows={totalRows}
            _onClick={_onClick}
           />
        </Grid>
      </Grid>
    )
}

export default TableAndSearch;
