import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
    },
  }));

const SearchField = (props) => {
    const classes = useStyles();
    return (
        <form className={classes.margin} onSubmit={(e) => {props.handleSubmitForm(e); props.setPage0()}}>
            <InputLabel htmlFor="input-with-icon-adornment">Station name</InputLabel>
            <Input
            id="input-with-icon-adornment"
            startAdornment={
                <InputAdornment position="start">
                <SearchIcon />
                </InputAdornment>
            }
            onChange={props.handleStateChange} 
            />
      </form>    )
}

export default SearchField;