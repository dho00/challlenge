import React from 'react';
import {
  PieChart, Pie, Legend, Cell, Tooltip,
} from 'recharts';

const PieChartComponent = (props) => {
  const { data } = props;
  return (
      <PieChart width={400} height={400}>
          <Pie
            data={data} 
            cx={300} 
            cy={200} 
            labelLine={false}
            outerRadius={80}
            dataKey="value"
          >
            {
              data.map((entry, index) => <Cell key={index} fill={entry.color}/>)
            }
          </Pie>
          <Tooltip />
          <Legend />
      </PieChart>
  );
}

export default PieChartComponent;
