export default {
    backendURL: process.env.REACT_APP_BACKEND_URL || (window.globalConfig && window.globalConfig.backendURL) || 'http://localhost:8000',
    backendWSURL: process.env.REACT_APP_WS_BACKEND_URL || (window.globalConfig && window.globalConfig.backendWSURL) || 'ws://localhost:8000/',
  };
  