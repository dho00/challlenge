import React from 'react';
import TopBar from 'components/TopBar';
import commonSettings from 'settings/common';
import logout from 'utils/user'

export default () => {
  
  const propsTopBar = {
    ...commonSettings.topBar,
    onLogout: () => {
      logout();
    },
  }

  return(
    <TopBar
      {...propsTopBar}
    />
  )
};
