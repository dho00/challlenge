import React from 'react';
import GeoMap from 'components/GeoMap';
import { QUERY_ALL_VELIBS } from 'queries';
import { Query } from "react-apollo";
import allvelibsAdapter from 'adapters/geoallvelibs.adapter';

const buildVariables = () => {
    return {
      orderBy: 'stationCode',
    }
}

export default (props) => {

  const {viewState, _onViewStateChange, _onHover, _renderTooltip} = props;
  
  return (
    <Query 
      query={QUERY_ALL_VELIBS}
      variables={buildVariables()}
    >
    {
        ({ loading, error, data }) => {
          if(error) return <div>ERROR 1</div>
          const adaptedTableProps = loading ? {
            data: []
          } : {
            data: allvelibsAdapter(data)
          }
      return (
          <GeoMap {...adaptedTableProps}
          viewState={viewState}
          _onViewStateChange={_onViewStateChange}
          _onHover={_onHover}
          _renderTooltip={_renderTooltip}
          />
          )
      }
    }
    </Query>
  )
}
