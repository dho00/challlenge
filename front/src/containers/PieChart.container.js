import React from 'react';
import { Query } from "react-apollo";
import { QUERY_AGG_STATION_STATE } from 'queries';
import PieChartComponent from 'components/PieChart';
import stationstateAdapter from 'adapters/stationstate.adapter';

export default () => {

  return (
    <Query 
      query={QUERY_AGG_STATION_STATE}
    >
    {
        ({ loading, error, data }) => {
            if(error) return <div>ERROR 1</div>
            const adaptedTableProps = loading ? {
              data: []
            } : {
              data: stationstateAdapter(data)
            }
        return (
            <PieChartComponent {...adaptedTableProps}/>
            )
        }
    }
    </Query>
  )
}
