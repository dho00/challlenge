import React, {useState} from 'react';
import { QUERY_ALL_VELIBS, QUERY_AGG_STATION_STATE } from 'queries';
import allvelibsAdapter from 'adapters/allvelibs.adapter';
import tableSettings from 'settings/pages/allVelibsTable';
import TableAndSearch from 'components/TableAndSearch';
import { useQuery } from '@apollo/react-hooks';

export default (props) => {
  
  const [sortedKey, setSortedKey] = useState('name');
  const [sortedType, setSortedType] = useState('asc');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [isAfter, setIsAfter] = useState(true);
  const [startCursor, setStartCursor] = useState(true);
  const [endCursor, setEndCursor] = useState(true);
  const [searchValueTemp, setSearchValueTemp] = useState('')
  const [searchValue, setSearchValue] = useState('')

  const handleChangePage = (event, newPage, cursors) => {
    newPage > page ? setIsAfter(true) : setIsAfter(false);
    setPage(newPage);
    setStartCursor(cursors.startCursor)
    setEndCursor(cursors.endCursor)
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setIsAfter(true)
    setStartCursor("")
    setEndCursor("")
    setPage(0)
  }

  const setPage0 = () => {
    setIsAfter(true)
    setStartCursor("")
    setEndCursor("")
    setPage(0)
  }
  const handleSubmitForm = (e) => {
    e.preventDefault();
    setSearchValue(searchValueTemp);
  }

  const handleStateChange = e => {
    setSearchValueTemp(e.target.value)
  }

  const buildVariables = () => {
    const order = sortedType === 'asc' ? '' : '-';
    if(isAfter) {
      return {
        orderBy: order+sortedKey,
        after: endCursor,
        first: rowsPerPage,
        stationNameContains:searchValue,
      }
    } else {
      return {
        orderBy: order+sortedKey,
        before: startCursor,
        last: rowsPerPage,
        stationNameContains:searchValue,
      }
    }
  }


  const { loading, error, data} = useQuery(QUERY_ALL_VELIBS, 
    { variables: buildVariables() });

  const isAccountLoading = loading;
  const selectedAccountsData = data;

  const isLoading = loading || isAccountLoading;
  
  const totalRows = isLoading ? 0 : selectedAccountsData.allVelibs.totalCount
  const adaptedTableProps = isLoading ? {
    ...tableSettings,
    isLoading: isLoading,
    rows: [],
  } : {
    ...tableSettings,
    tableHeader: {
      ...tableSettings.tableHeader,
      onSort: (v) => {
        v && v.sort_key ? setSortedKey(v.sort_key) : setSortedKey(null);
        v && v.sort_type ? setSortedType(v.sort_type) : setSortedType(null);
        v && setStartCursor("");
        v && setEndCursor("");
        v && setIsAfter(true);
        v && setPage(0);
      },
      sorting: {
        sort_type: sortedType,
        sort_key: sortedKey,
      },
    },
    tableData: {
      rows:allvelibsAdapter(selectedAccountsData),
    },
    cursors: {
      startCursor: selectedAccountsData.allVelibs.pageInfo.startCursor,
      endCursor: selectedAccountsData.allVelibs.pageInfo.endCursor,
    }
  }
  return (
    <TableAndSearch 
      {...adaptedTableProps }
      page={page}
      rowsPerPage={rowsPerPage}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      totalRows={totalRows}
      _onClick={props._onClick}
      handleSubmitForm={handleSubmitForm}
      handleStateChange={handleStateChange}
      setPage0={setPage0}
    />

  )
}
