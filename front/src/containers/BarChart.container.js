import React from 'react';
import { Query } from "react-apollo";
import { QUERY_AGG_VELIB_TYPE } from 'queries';
import BarChartComponent from 'components/BarChart';
import velibtypeAdapter from 'adapters/velibtype.adapter';

export default () => {

  return (
    <Query 
      query={QUERY_AGG_VELIB_TYPE}
    >
    {
        ({ loading, error, data }) => {
            if(error) return <div>ERROR 1</div>
            const adaptedTableProps = loading ? {
              data: []
            } : {
              data: velibtypeAdapter(data)
            }
        return (
            <BarChartComponent {...adaptedTableProps}/>
            )
        }
    }
    </Query>
  )
}
