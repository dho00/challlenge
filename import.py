from pyspark.sql import SparkSession
from pyspark.sql import Row

# warehouse_location points to the default location for managed databases and tables
warehouse_location = abspath('spark-warehouse')

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL Hive integration example") \
    .config("spark.sql.warehouse.dir", warehouse_location) \
    .enableHiveSupport() \
    .getOrCreate()

# Queries are expressed in HiveQL
sqlDF = spark.sql("SELECT * FROM event_table").show()


sqlDF.toDF().write.format('jdbc').options(
      url='jdbc:%s' % url,
      driver='org.postgresql.Driver',
      dbtable='pyspark_user',
      user='postgres',
      password='').mode('append').save()