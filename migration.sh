#!/bin/bash
local() {
  ./venv/bin/python3.7 challenge/manage.py makemigrations
  export LAST_FILE=$(./venv/bin/python3.7 challenge/manage.py makemigrations --empty app| grep -o challenge/app/migrations/.*.py$)
  echo $LAST_FILE
  sed -i '2 i\from django.contrib.postgres.operations import UnaccentExtension' $LAST_FILE
  sed -i "s/operations = \[/operations = \[ UnaccentExtension()/g" $LAST_FILE
  ./venv/bin/python3.7 challenge/manage.py migrate
}


case "$1" in
  local)
    local
    ;;
esac
