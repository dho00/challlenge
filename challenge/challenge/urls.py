"""challenge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, include 
from graphene_django.views import GraphQLView
from graphql.backend import GraphQLCoreBackend
from django.views.generic import TemplateView
from app.views import LoginView, LogoutView
from .logging_middleware import LoggingMiddleware

class GraphQLCustomCoreBackend(GraphQLCoreBackend):
    def __init__(self, executor=None):
        # type: (Optional[Any]) -> None
        super().__init__(executor)
        self.execute_params['allow_subscriptions'] = True

urlpatterns = [
    re_path(r'admin/?', admin.site.urls),
    re_path(
        r'^graphql/?',
        GraphQLView.as_view(graphiql=True,
                            backend=GraphQLCustomCoreBackend(),
                            middleware=[LoggingMiddleware()])),
    re_path('auth/login/?', LoginView.as_view()),
    re_path('auth/logout/?', LogoutView.as_view()),
    re_path(r'^', TemplateView.as_view(template_name='index.html'))
]
