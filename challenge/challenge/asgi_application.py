import os
import channels
import django
import channels_graphql_ws

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'challenge.settings_dev')

django.setup()

from app.schema import schema
import channels.auth


class MyGraphqlWsConsumer(channels_graphql_ws.GraphqlWsConsumer):

    schema = schema

    async def on_connect(self, payload):
        self.scope["user"] = await channels.auth.get_user(self.scope)


application = channels.routing.ProtocolTypeRouter({
    "websocket":
    channels.auth.AuthMiddlewareStack(
        channels.routing.URLRouter(
            [django.urls.path("ws/", MyGraphqlWsConsumer)]))
})
