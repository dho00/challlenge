from app.utils import log_query


class LoggingMiddleware(object):
    def resolve(self, next, root, info, **args):
        if not root and info.operation.operation == 'query':
            arguments = {}
            arguments["args"] = info.operation.selection_set.selections[
                0].arguments
            arguments["variables_values"] = info.variable_values
            log_query(info.context.user.id, info.field_name, arguments)
        return next(root, info, **args)
