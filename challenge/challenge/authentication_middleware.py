from django.http.response import HttpResponse
from re import compile

AuthRequiredRoutes = [
    r'^graphql/?',
]


class AuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        if any((compile(m).match(request.path_info.lstrip('/'))
                for m in AuthRequiredRoutes
                )) and not request.user.is_authenticated:
            return HttpResponse('you need to log in', status=403)
        return None
