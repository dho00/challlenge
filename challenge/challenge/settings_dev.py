from challenge.settings_base import *
import os

ALLOWED_HOSTS = ['*']

# Destination of 'collectstatic'
STATIC_ROOT = '/home/docker/volatile/static'

# FS Path for django serving static files
STATICFILES_DIRS = [
    os.path.join(os.path.dirname(BASE_DIR), 'front', 'build', 'static'),
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(os.path.dirname(BASE_DIR), 'front', 'build')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
