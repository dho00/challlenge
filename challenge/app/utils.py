import logging
import pytz
from datetime import datetime
from django.conf import settings
from graphql.language.ast import ListValue
import json

def log_query(user_id, operation_name, args):
    logging.info({
        'user_id': user_id,
        'operationName': operation_name,
        'args': format_args(args)
    })

def format_args(arguments):
    args_dict = dict()
    if arguments["variables_values"]:
        args_dict = arguments["variables_values"]
    else:
        for arg in arguments["args"]:
            if isinstance(arg.value, ListValue):
                args_dict[arg.name.value] = arg.value.values
            else:
                args_dict[arg.name.value] = arg.value.value
    return args_dict

