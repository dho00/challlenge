from django.test import TestCase as DjangoTestCase
from snapshottest import TestCase as SnapshotTestCase
from graphene.test import Client
from .utils import request_superuser
from ..schema import schema
from .queries import query_consent_asked, query_consent_asked_with_consent, query_consent_given_with_consent, query_consent_given, query_pageview_with_consent, query_page_views, query_unique_user
# Create your tests here.

class ChallengeTest(DjangoTestCase, SnapshotTestCase):
    # Fixtures
    fixtures = ['sample']

    def test_query_consent_asked(self):
        client = Client(schema)
        executed = client.execute(query_consent_asked,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

    def test_query_consent_asked_with_consent(self):
        client = Client(schema)
        executed = client.execute(query_consent_asked_with_consent,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

    def test_query_consent_given_with_consent(self):
        client = Client(schema)
        executed = client.execute(query_consent_given_with_consent,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

    def test_query_consent_given(self):
        client = Client(schema)
        executed = client.execute(query_consent_given,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

    def test_query_pageview_with_consent(self):
        client = Client(schema)
        executed = client.execute(query_pageview_with_consent,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)


    def test_query_page_views(self):
        client = Client(schema)
        executed = client.execute(query_page_views,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

    def test_query_unique_user(self):
        client = Client(schema)
        executed = client.execute(query_unique_user,
                                  context_value=request_superuser(self))
        self.assertMatchSnapshot(executed)

