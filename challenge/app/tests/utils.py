from django.contrib.auth.models import Permission
from django.test import RequestFactory
from django.contrib.auth.models import User

def request_superuser(self):
    request = RequestFactory().get('/')
    if User.objects.filter(username="admin").exists():
        self.user = User.objects.get(username="admin")
    else:
        self.user = User.objects.create_superuser(username='admin',
                                                  email='admin@example.com',
                                                  password='admin')
    self.client.login(username=self.user.username, password="admin")
    request.user = self.user
    return request

