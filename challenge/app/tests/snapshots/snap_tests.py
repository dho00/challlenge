# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['ChallengeTest::test_query_consent_asked 1'] = {
    'data': {
        'getConsentasked': [
            {
                'count': 2,
                'timestamp': '2020-01-29'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_consent_asked_with_consent 1'] = {
    'data': {
        'getConsentaskedWithConsent': [
            {
                'count': 1,
                'timestamp': '2020-01-29'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_consent_given 1'] = {
    'data': {
        'getConsentgiven': [
            {
                'count': 3,
                'timestamp': '2020-01-25'
            },
            {
                'count': 2,
                'timestamp': '2020-01-29'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_consent_given_with_consent 1'] = {
    'data': {
        'getConsentgivenWithConsent': [
            {
                'count': 2,
                'timestamp': '2020-01-25'
            },
            {
                'count': 1,
                'timestamp': '2020-01-29'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_page_views 1'] = {
    'data': {
        'getPageviews': [
            {
                'count': 1,
                'timestamp': '2020-01-15'
            },
            {
                'count': 2,
                'timestamp': '2020-01-25'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_pageview_with_consent 1'] = {
    'data': {
        'getPageviewsWithConsent': [
            {
                'count': 2,
                'timestamp': '2020-01-25'
            }
        ]
    }
}

snapshots['ChallengeTest::test_query_unique_user 1'] = {
    'data': {
        'getUniqueUser': [
            {
                'count': 1,
                'timestamp': '2020-01-10'
            },
            {
                'count': 1,
                'timestamp': '2020-01-15'
            },
            {
                'count': 2,
                'timestamp': '2020-01-25'
            },
            {
                'count': 2,
                'timestamp': '2020-01-29'
            }
        ]
    }
}
