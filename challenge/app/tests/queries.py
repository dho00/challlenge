query_consent_asked = '''
query getConsentasked {
    getConsentasked {
        timestamp
        count
    }
}
'''

query_consent_asked_with_consent = '''
query getConsentaskedWithConsent {
    getConsentaskedWithConsent {
        timestamp
        count
      }
}
'''

query_consent_given_with_consent = '''
query getConsentgivenWithConsent {
    getConsentgivenWithConsent {
        timestamp
        count
    }
}
'''

query_consent_given = '''
query getConsentgiven {
    getConsentgiven {
        timestamp
        count
    }      
}
'''

query_pageview_with_consent = '''
query getPageviewsWithConsent {
    getPageviewsWithConsent {
		timestamp
        count
  }
}
'''

query_page_views = '''
query getPageviews {
  getPageviews {
		timestamp
    count
  }
}
'''

query_unique_user = '''
query getUniqueUser {
  getUniqueUser {
    timestamp
    count
  } 
}
'''

