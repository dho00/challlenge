# from django.db import models
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.gis.db import models as GisModels

import graphene
from graphene_django.converter import convert_django_field
# from django_mysql.models import JSONField

@convert_django_field.register(GisModels.GeometryField)
def convert_json_field_to_string(field, registry=None):
    return graphene.String()

class Client(models.Model):
    consent = models.BooleanField()
    contry = models.CharField(max_length=100)

class Data(models.Model):
    timestamp = models.DateField()
    action_type = models.CharField(max_length=100)
    domain = models.CharField(max_length=100)
    user_id = models.CharField(max_length=128)
    user_consent = models.BooleanField()
    user_country = models.CharField(max_length=100)

class Velib(models.Model):
    station_code = models.CharField(max_length=100) # code  de station
    station_name = models.CharField(max_length=100) # nom des stations
    station_state = models.CharField(max_length=100) # Etat des stations
    nbfreedock = models.IntegerField() #nb de borne disponible
    overflowactivation = models.CharField(max_length=100) # PARK + activation
    nbedock = models.IntegerField() # nbr de borne disponible en station
    nbbikeoverflow = models.IntegerField() # Nombre vélo en PARK+
    nbbike = models.IntegerField() # nbr velo mecanique
    nbebike = models.IntegerField() # nbr velo electrique
    creditcard = models.CharField(max_length=100) # achat possible en station
    kioskstate = models.CharField(max_length=100) # Etat du Totem
    # geo = GisModels.GeometryField(models.FloatField()) # coordinate
    geo = models.PointField()
    # geo = ArrayField(models.FloatField()) # coordinate
    timestamp = models.DateField()


    def __str__(self):
        return '%s %s' % (self.station_name, self.geo)