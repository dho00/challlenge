from itertools import islice
import pandas as pd
from app.models import Velib
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point, GEOSGeometry
# from .models import Client, Data, Velib

# run python challenge/manage.py shell
# ensuite copie colle le fichier
df = pd.read_csv('velib-disponibilite-en-temps-reel.csv', sep=';')

objs = []
for index, row in df.iterrows():
    geo_split = row[11].split(',')
    pnt = Point(float(geo_split[0]), float(geo_split[1]), srid=4326)
    new_values = {
        'station_code': row[0], 
        'station_name': row[1],
        'station_state': row[2],
        'nbfreedock': int(row[5]),
        'overflowactivation': row[9],
        'nbedock': int(row[4]),
        'nbbikeoverflow': int(row[10]),
        'nbbike': int(row[6]),
        'nbebike': int(row[7]),
        'creditcard': row[8],
        'kioskstate': row[3],
        'geo': pnt,
        'timestamp': row[12],
    }
    obj = Velib(**new_values)
    objs.append(obj)

Velib.objects.bulk_create(objs)

# batch_size = 100
# # objs = (Entry(headline='Test %s' % i) for i in range(1000))
# while True:
#     batch = list(islice(objs, batch_size))
#     if not batch:
#         break
#     Velib.objects.bulk_create(batch, batch_size)