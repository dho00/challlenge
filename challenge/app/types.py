from graphene_django.types import DjangoObjectType
from .models import Client, Data, Velib
from graphene import relay, Int, Field, Connection, ObjectType
from django_filters import FilterSet, OrderingFilter, CharFilter
from graphene_django.filter import DjangoFilterConnectionField
from graphene.types.generic import GenericScalar
from graphene.types.datetime import DateTime

class GenericScalarType(ObjectType):
    result = GenericScalar()

class KeyValueType(ObjectType):
    name = GenericScalar()
    value = GenericScalar()

class VelibConnection(Connection):
    class Meta:
        abstract = True

    total_count = Int()
    edge_count = Int()

    def resolve_total_count(self, info, **kwargs):
        return self.length
    def resolve_edge_count(self, info, **kwargs):
        return len(self.edges)

class VelibType(DjangoObjectType):
    geo = GenericScalar()
    class Meta:
        model = Velib

class VelibNodeType(DjangoObjectType):
    class Meta:
        model = Velib
        filter_fields = ['station_name', 'station_state']
        interfaces = (relay.Node, )
        connection_class = VelibConnection

class VelibFilter(FilterSet):
    class Meta:
        model = Velib
        fields = ['station_name', 'station_state']

    station_name_contains = CharFilter(field_name='station_name', lookup_expr='icontains')

    order_by = OrderingFilter(
        fields=(
            ('station_code', 'station_code'),
            ('station_name', 'station_name'),
            ('station_state', 'station_state'),
            ('nbbike', 'nbbike'),
            ('nbebike', 'nbebike'),
        )
    )

