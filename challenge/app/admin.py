from django.contrib import admin
from .models import Data, Client, Velib
# Register your models here.

admin.site.register(Data)
admin.site.register(Client)
admin.site.register(Velib)
