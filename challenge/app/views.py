from django.shortcuts import render
from django.http import HttpResponse
from .models import Data
from prometheus_client import Info, Gauge
from prometheus_client import CONTENT_TYPE_LATEST, generate_latest
import time
from django.contrib.auth import login, logout
from rest_framework import generics
from rest_framework.response import Response
from .serializers import LoginUserSerializer, UserSerializer
# Create your views here.

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")



class LoginView(generics.GenericAPIView):
    serializer_class = LoginUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data
            login(request, user)
            return Response({
                "user":
                UserSerializer(user,
                               context=self.get_serializer_context()).data
            })
        else:
            return Response({"errors": "Wrong credentials"}, status=401)

class LogoutView(generics.GenericAPIView):
    def post(self, request, *args, **kwargs):
        logout(request)
        return Response({"msg": "logout successful"}, status=200)