import graphene
from graphene_django.filter import DjangoFilterConnectionField
from django.db.models import Count, Avg, Sum, F
from django.db.models.functions import Greatest, TruncDate
from .models import Client, Data, Velib
from .types import GenericScalarType, VelibType, VelibNodeType, VelibConnection, VelibFilter, KeyValueType
from graphql import GraphQLError
from django_graphene_permissions import permissions_checker
from django_graphene_permissions.permissions import IsAuthenticated
import graphene_django_optimizer as gql_optimizer
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point, GEOSGeometry
from django.contrib.gis.measure import D

class Query(graphene.ObjectType):


    ###########
    ## Velib ##
    ###########

    all_velibs = DjangoFilterConnectionField(VelibNodeType, filterset_class=VelibFilter)

    get_nearest = graphene.List(VelibType)

    get_agg_station_state = graphene.List(KeyValueType)

    get_agg_velib_type = graphene.List(KeyValueType)

                            ###############
                            ## RESOLVER  ##
                            ###############

    @permissions_checker([IsAuthenticated])
    def resolve_get_nearest(self, info, **kwargs):
        # a = Point(48.8566, 2.2522, srid=4326)
        pnt = GEOSGeometry('POINT(-96.876369 29.905320)', srid=4326)
        return Velib.objects.annotate(
            distance=Distance('geo',pnt)
        ).order_by('distance').all()[:10]


    @permissions_checker([IsAuthenticated])
    def resolve_get_agg_velib_type(self, info, **kwargs):
        sum_nbbike = Velib.objects.aggregate(value=Sum('nbbike'))
        sum_nbebike = Velib.objects.aggregate(value=Sum('nbebike'))
        return [
            {
                'name': 'bike',
                'value': sum_nbbike.get('value'),
            },{
                'name': 'ebike',
                'value': sum_nbebike.get('value'),
            }
        ]

    @permissions_checker([IsAuthenticated])
    def resolve_get_agg_station_state(self, info, **kwargs):
        data = Velib.objects.values(name=F('station_state')).annotate(value=Count('station_state'))
        return data

    @permissions_checker([IsAuthenticated])
    def resolve_all_velibs(self, info, **kwargs):
        data = gql_optimizer.query(Velib.objects.all(), info)
        return data
